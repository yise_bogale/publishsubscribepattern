#Publish Subscribe Pattern

##By `Yisehak Bogale`
- ATR/8083/09
- Section 3

##Instructions

- compile all the projects, then run `user1.c` first
- on user 1, send a text that starts with `@` to reach user 2, `#` to reach user 3, and `!` to reach back user 1
- whichever program is reached will recieve the text written afterwards and display it, and they are able to send too