#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>

#define SHM_SIZE 27

int main() {
    char c;
    int shm_id;
    key_t key;
    char *shm, *s;
    key = 5678;
    if ((shm_id = shmget(key, SHM_SIZE, IPC_CREAT | 0666)) < 0) {
        printf("shmget error \n");
        exit(1);
    }

    if ((shm = shmat(shm_id, NULL, 0)) == (char *) - 1) {
        printf("shmat error\n");
        exit(1);
    }
    while (*shm != '*') {

        char *a = shm;

        while (*a != '#')
            sleep(1);

        printf("\n!! --------- !! \nMesssage: ");
        for (s = shm + 1; *s != '\0'; s++)
            putchar(*s);

        s = shm;
        printf("User 3: ");
        fgets(s, SHM_SIZE, stdin);
        printf("\n");
    }


//    *shm = '*';

    shmdt(shm);
    shmctl (shm_id, IPC_RMID, NULL);
    return 0;
}